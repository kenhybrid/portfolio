// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore, enableIndexedDbPersistence } from "firebase/firestore";
import { getAuth } from "firebase/auth";
import { getStorage } from "firebase/storage";
const firebaseConfig = {
  apiKey: "AIzaSyAxm8eKv2_UETXHCnMzNKIC7w6B8uLZRKo",
  authDomain: "portfolio-eec85.firebaseapp.com",
  projectId: "portfolio-eec85",
  storageBucket: "portfolio-eec85.appspot.com",
  messagingSenderId: "1066720549278",
  appId: "1:1066720549278:web:3e71215e9fb93f96dd5e8f",
  measurementId: "G-4JEG8Y6RXB",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth(app);
enableIndexedDbPersistence(db);
const storage = getStorage(app);
export { db, storage, auth };
