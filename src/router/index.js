import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Technologies from "../views/Technologies.vue";
import store from "../store/index.js";
const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/projects",
    name: "Projects",
    component: () =>
      import(/* webpackChunkName: "Projects" */ "../views/Projects.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "Login" */ "../views/Login.vue"),
  },
  {
    path: "/create",
    name: "Create",
    meta: { requiresAuth: true },
    component: () =>
      import(/* webpackChunkName: "Create" */ "../views/Create.vue"),
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    meta: { requiresAuth: true },
    component: () =>
      import(/* webpackChunkName: "Dashboard" */ "../views/Dashboard.vue"),
  },
  {
    path: "/technologies",
    name: "Technologies",
    component: Technologies,
  },
  {
    path: "/socials",
    name: "Socials",
    component: () =>
      import(/* webpackChunkName: "Socials" */ "../views/Socials.vue"),
  },
  {
    path: "/:catchAll(.*)",
    name: "404",
    component: () => import(/* webpackChunkName: "404" */ "../views/404.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior: function (to, _from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    if (to.hash) {
      return { el: to.hash, behavior: "smooth" };
    } else {
      // console.log("moving to top of the page");
      window.scrollTo(0, 0);
    }
  },
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.getters.getAuthstate == true) {
      next();
      return;
    }
    next("/login");
  } else {
    next();
  }
});
export default router;
