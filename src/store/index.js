import { createStore, createLogger } from "vuex";
import createPersistedState from "vuex-persistedstate";
import { db, storage, auth } from "../firebase";
import router from "../router";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import {
  collection,
  getDocs,
  addDoc,
  query,
  orderBy,
  doc,
  deleteDoc,
} from "@firebase/firestore";
import { signInWithEmailAndPassword, signOut } from "@firebase/auth";
export default createStore({
  plugins: [
    createLogger(),
    createPersistedState({
      key: "portfolio",
    }),
  ],
  state: {
    projects: [],
    loading: false,
    authstate: false,
  },
  getters: {
    getProjects(state) {
      return state.projects;
    },
    getLoading(state) {
      return state.loading;
    },
    getAuthstate(state) {
      return state.authstate;
    },
  },
  mutations: {
    setProjects(state, payload) {
      state.projects = payload;
    },
    setLoading(state, payload) {
      state.loading = payload;
    },
    addProject(state, payload) {
      state.projects.unshift(payload);
    },
    deleteProject(state, payload) {
      state.projects.splice(payload.index, 1);
    },
    Logedin(state, payload) {
      state.authstate = payload;
    },
  },
  actions: {
    async getProjects({ commit }) {
      commit("setLoading", true);
      try {
        const ref = collection(db, "projects");
        const q = query(ref, orderBy("date", "asc"));
        const documentSnapshots = await getDocs(q);
        const projects = [];
        documentSnapshots.forEach((doc) => {
          const data = {
            id: doc.id,
            title: doc.data().title,
            link: doc.data().link,
            avatar: doc.data().avatar,
          };
          projects.push(data);
        });
        commit("setProjects", projects);
        commit("setLoading", false);
      } catch (error) {
        console.log("Error getting documents: ", error);
        commit("setLoading", false);
      }
    },
    async addProject({ commit }, payload) {
      commit("setLoading", true);

      try {
        const imagesRef = ref(storage, `images/${payload.file.name}`);
        const uploadTask = uploadBytesResumable(imagesRef, payload.file);
        uploadTask.on(
          "state_changed",
          (snapshot) => {
            // Observe state change events such as progress, pause, and resume
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            const progress =
              (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log("Upload is " + progress + "% done");
            switch (snapshot.state) {
              case "paused":
                console.log("Upload is paused");
                break;
              case "running":
                console.log("Upload is running");
                break;
            }
          },
          (error) => {
            // Handle unsuccessful uploads
            console.log(error);
          },
          () => {
            // Handle successful uploads on complete
            // For instance, get the download URL: https://firebasestorage.googleapis.com/...
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
              const data = {
                title: payload.title,
                link: payload.link,
                avatar: downloadURL,
                date: payload.date,
              };
              const projectsref = collection(db, "projects");
              addDoc(projectsref, data)
                .then((doc) => {
                  const project = {
                    id: doc.id,
                    title: payload.title,
                    avatar: downloadURL,
                    date: payload.date,
                    link: payload.link,
                  };
                  commit("addProject", project);
                  commit("setLoading", false);
                  router.push("/dashboard");
                })
                .catch((error) => {
                  console.log(error);
                });
            });
          }
        );
      } catch (error) {
        console.log("Error getting documents: ", error);
        commit("setLoading", false);
      }
    },
    async deleteProject({ commit }, payload) {
      commit("setLoading", true);

      try {
        await deleteDoc(doc(db, "projects", payload.id));
        const object = {
          index: payload.index,
        };
        commit("deleteProject", object);
        commit("setLoading", false);
      } catch (error) {
        console.log("Error deleting documents: ", error);
        commit("setLoading", false);
      }
    },
    async Login({ commit }, payload) {
      commit("setLoading", true);

      try {
        const res = await signInWithEmailAndPassword(
          auth,
          payload.email,
          payload.password
        );
        console.log(res);
        router.push("/dashboard");

        commit("Logedin", true);
        commit("setLoading", false);
      } catch (error) {
        console.log("Error deleting documents: ", error);
        commit("setLoading", false);
      }
    },
    async LogOut({ commit }) {
      commit("setLoading", true);

      try {
        await signOut(auth);

        router.push("/");

        commit("Logedin", false);

        commit("setLoading", false);
      } catch (error) {
        console.log("Error deleting documents: ", error);
        commit("setLoading", false);
      }
    },
  },
});
