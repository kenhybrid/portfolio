Content management capabilities
Promotion and discount code tools
An easy-to-use checkout
Search engine optimized code and layout
Reporting tools and custom report features
An integrated blog or articles section
Email marketing features or integration
Multiple payment options (Credit card, PayPal, PO, Terms, etc.)
The ability to scale and add new features